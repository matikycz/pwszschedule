class CreateUpdates < ActiveRecord::Migration
  def change
    create_table :updates do |t|
      t.datetime :time, null: false, default: DateTime.now
      t.integer :inserts, null: false, default: 0
      t.integer :updates, null: false, default: 0
      t.integer :deletes, null: false, default: 0

      t.timestamps null: false
    end
  end
end
