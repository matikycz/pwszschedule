class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.datetime :startTime, null: false
      t.datetime :endTime, null: false
      t.string :subject, null: false
      t.string :teacher, null: false
      t.string :place, null: false
      t.integer :group, null: false
      t.datetime :version, null: false, default: DateTime.now

      t.timestamps null: false
    end
  end
end
