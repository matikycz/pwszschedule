class Lesson
  attr_reader :day, :startTime, :endTime, :subject, :teacher, :place, :group
  attr_writer :dat, :startTime, :endTime, :subject, :teacher, :place, :group

  def initialize(day, time, subject, teacher, place, group)
    @day = day
    @startTime = time.split('-')[0]
    @endTime = time.split('-')[1]
    @subject = subject
    @teacher = teacher
    @place = place
    @group = group
  end
end
