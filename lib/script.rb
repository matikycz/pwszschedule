require_relative 'updater'
require_relative 'lesson'

updater = Updater.new
updater.update_schedule
updater.update_version
updater.send_mail_notifier
