require 'open-uri'
require_relative 'lesson'

class ScheduleDownloader
  attr_reader :lessons
  attr_writer :lessons

  def initialize
    @lessons = Array.new
  end

  def download
    file = open('http://www.plan.pwsz.legnica.edu.pl/checkSpecjalnoscStac.php?specjalnosc=s1MPED', "charset" => "utf-8")
    content = file.read.encode('utf-8')
    begin_index = content.index('<table')
    end_index = content.index('</table>')
    main_table = content[begin_index..end_index]
    days_array = main_table.split('<td class=nazwaDnia ')
    @lessons = Array.new
    for i in 1...days_array.size
      lessons_array = days_array[i].split('<td class=godzina>')
      day = get_value(lessons_array[0])
      day = day[day.index(' ')+1...day.size]
      for j in 1...lessons_array.size
        possitions = lessons_array[j].split('<td')
        time = possitions[0][0...possitions[0].index('<')]
        groups = possitions.size/3
        for k in 0...groups
          subject = get_value possitions[1 + 3*k]
          teacher = get_value possitions[2 + 3*k]
          place = get_value possitions[3 + 3*k]
          @lessons.push Lesson.new(day, time, subject, teacher, place, k+1)
        end
      end
    end
  end

  private
  def get_value content 
    content[content.index('>')+1...content.index('<')]
  end
end
