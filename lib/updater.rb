require_relative 'lesson'
require_relative 'schedule_downloader'

class Updater

  attr_reader :inserts, :updates, :deletes, :errors
  attr_writer :inserts, :updates, :deletes, :errors

  def initialize
    @inserts = 0
    @updates = 0
    @deletes = 0
    @errors = Array.new
  end

  def update_schedule
    schedule_downloader = ScheduleDownloader.new
    schedule_downloader.download
    lessons = schedule_downloader.lessons
    schedules = Schedule.all
    lessons.each do |lesson|
      found = schedules.select { |schedule| schedule.startTime == DateTime.parse(lesson.day + " " + lesson.startTime) and schedule.group == lesson.group}

      if found.size == 0 
        @inserts += 1
        Schedule.create(startTime: DateTime.parse(lesson.day + " " + lesson.startTime), endTime: DateTime.parse(lesson.day + " " + lesson.endTime), subject: lesson.subject, teacher: lesson.teacher, place: lesson.place, group: lesson.group, version: DateTime.now)
      elsif found.size == 1 
        @updates += 1
        found[0].update(startTime: DateTime.parse(lesson.day + " " + lesson.startTime), endTime: DateTime.parse(lesson.day + " " + lesson.endTime), subject: lesson.subject, teacher: lesson.teacher, place: lesson.place, group: lesson.group, version: DateTime.now)
      else
        @errors.push lesson
      end 
    end
  end

  def update_version
    Update.create(inserts: @inserts, updates: @updates, deletes: @deletes, time: DateTime.now) 
  end

  def send_mail_notifier
    NotifierMailer.update_notify(@inserts, @updates, @deletes, @errors, DateTime.now).deliver_now
  end
end
