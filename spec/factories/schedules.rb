# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :schedule do
    startTime DateTime.parse('2016-04-21 08:15')
    endTime DateTime.parse('2016-04-21 09:45')
    subject 'subject'
    teacher 'teacher'
    place 'place'
    group 1
    version DateTime.now
  end
end
