require 'rails_helper'

RSpec.describe Update, type: :model do
  context 'valid' do
    it 'valid with all params set' do
      update = Update.new(time: DateTime.now, inserts: 1, updates: 1, deletes: 0)
      update.valid?
      expect(update).to be_valid
    end

   it 'valid without time' do 
     update = Update.new
     update.valid?
     expect(update.errors[:time]).to eq []
   end 

   it 'valid without inserts' do 
     update = Update.new
     update.valid?
     expect(update.errors[:inserts]).to eq []
   end 

   it 'valid without updates' do 
     update = Update.new
     update.valid?
     expect(update.errors[:updates]).to eq []
   end 

   it 'valid without deletes' do 
     update = Update.new
     update.valid?
     expect(update.errors[:deletes]).to eq []
   end 
  end
end
