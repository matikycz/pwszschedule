require 'rails_helper'

RSpec.describe Schedule, type: :model do
  context 'valid params' do
    it 'is valid with all set params' do
      schedule = Schedule.new(startTime: '2015-10-01 8:15', endTime: '2015-10-01 9:45', subject: 'Test subject', teacher: 'Test teacher', place: 'Test place', group: 1, version: DateTime.now)
      schedule.valid?
      expect(schedule).to be_valid
    end

    it 'is valid without version' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:version]).to eq []
    end
  end

  context 'invalid params' do
    it 'is invalid without startTime' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:startTime]).to include("can't be blank")
    end

    it 'is invalid without endTime' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:endTime]).to include("can't be blank")
    end

    it 'is invalid without subject' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:subject]).to include("can't be blank")
    end

    it 'is invalid without teacher' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:teacher]).to include("can't be blank")
    end

    it 'is invalid without place' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:place]).to include("can't be blank")
    end

    it 'is invalid without group' do
      schedule = Schedule.new
      schedule.valid?
      expect(schedule.errors[:group]).to include("can't be blank")
    end
  end
end
