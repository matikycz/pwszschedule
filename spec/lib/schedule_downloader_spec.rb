require 'rails_helper'
require 'schedule_downloader'

describe ScheduleDownloader do
  describe '#download' do
    it 'download all schedule lessons' do
      schedule_downloader = ScheduleDownloader.new
      schedule_downloader.download
      expect(schedule_downloader.lessons.size).to eq 728
    end
  end
end 
