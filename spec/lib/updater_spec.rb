require 'rails_helper'
require 'updater'

RSpec.describe Updater do
  context '#update_schedule' do
    let(:updater) { Updater.new }
    
    it 'sum of inserts, updates and deletes equal to downloaded lessons count' do
      updater.update_schedule
      inserts = updater.inserts
      updates = updater.updates
      deletes = updater.deletes
      sum = inserts + updates + deletes
      expect(sum). to eq 728
    end

    it 'change schedules count by inserts' do
      count = Schedule.count
      updater.update_schedule
      inserts = updater.inserts
      deletes = updater.deletes
      expect(Schedule.count).to eq count + inserts - deletes
    end

    it 'should update record' do
      FactoryGirl.create(:schedule)
      updater.update_schedule
      updates = updater.updates
      expect(updates).to be >= 1
    end
  end

  context '#update_version' do
    let(:updater) { Updater.new }

    it 'add new version to log' do
      updater.update_schedule
      expect{ updater.update_version }.to change{ Update.count }.by(1)
    end
  end

  context '#send_mail_notifier' do
    let(:updater) { Updater.new }

    it 'send correct mail notification' do
      updater.update_schedule
      updater.update_version
      updater.send_mail_notifier
      expect(true).to be_truthy
    end
  end
end
