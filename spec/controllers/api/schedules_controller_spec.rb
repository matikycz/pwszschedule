require 'rails_helper'

RSpec.describe Api::V1::SchedulesController, type: :controller do
  describe 'GET #index' do
    subject { get :index, format: :json }
    before { FactoryGirl.create(:schedule) }

    context 'valid index' do
      it 'return all schedules' do
        expect(subject)
        expect_json('0', startTime: '2016-04-21 08:15', endTime: '2016-04-21 09:45', subject: 'subject', teacher: 'teacher', place: 'place', group: 1)
      end
    end
  end
end
