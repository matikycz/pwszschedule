require 'lesson'

class NotifierMailerPreview < ActionMailer::Preview
  def update_notify_preview
    errors = Array.new
    errors.push Lesson.new('2015-10-01', '8:15-9:45', 'Przedmiot', 'Nauczyciel', 'C120', 1)
    NotifierMailer.update_notify(10, 10, 10, errors, DateTime.now)
  end
end
