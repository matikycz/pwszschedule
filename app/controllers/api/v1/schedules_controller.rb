module Api
  module V1
    class SchedulesController < Api::V1::BaseController
      def index
        schedules = Schedule.all
        schedules = schedules.map do |schedule|
          ::V1::ScheduleRepresenter.new(schedule).basic
        end
        render json: schedules
      end
    end
  end
end
