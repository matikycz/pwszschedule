class V1::ScheduleRepresenter
  def initialize(schedule)
    @schedule = schedule
  end

  def basic
    {
      id: @schedule.id,
      startTime: I18n.l(@schedule.startTime, format: "%Y-%m-%d %H:%M").humanize,
      endTime: I18n.l(@schedule.endTime, format: "%Y-%m-%d %H:%M").humanize,
      subject: @schedule.subject,
      teacher: @schedule.teacher,
      place: @schedule.place,
      group: @schedule.group,
      version: @schedule.version.to_time.to_i
    }
  end
end
