class ApplicationMailer < ActionMailer::Base
  default from: "powiadomienia@sitarczyk.eu"
  layout 'mailer'
end
