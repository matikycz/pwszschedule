class NotifierMailer < ApplicationMailer
  def update_notify(inserts, updates, deletes, errors, time)
    @inserts = inserts
    @updates = updates
    @deletes = deletes
    @errors = errors
    @time = time
    mail(to: 'matikycz@sitarczyk.eu', subject: 'Aktualizacja planu PWSZ')
  end
end
