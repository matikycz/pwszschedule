class Update < ActiveRecord::Base
  validates :time, presence: true
  validates :inserts, presence: true
  validates :updates, presence: true
  validates :deletes, presence: true
end
