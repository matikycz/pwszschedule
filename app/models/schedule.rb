class Schedule < ActiveRecord::Base
  validates :startTime, presence: true
  validates :endTime, presence: true
  validates :subject, presence: true
  validates :teacher, presence: true
  validates :place, presence: true
  validates :group, presence: true
  validates :version, presence: true
end
